package com.Developers;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import javax.sound.midi.VoiceStatus;

import org.apache.catalina.filters.AddDefaultCharsetFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import ClasesGenericas.Socio;
import ClasesGenericas.Voluntario;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AppOngApplicationTests {

	@Test
	public void agregarSocio() {
		
		ArrayList<Socio>socio = new ArrayList<>();
		assertEquals(0,socio.size());
		socio.add(null);
		socio.add(null);
		assertEquals(2,socio.size());
		
	}
	
	@Test
	
	public void eliminarSocio() {
		ArrayList<Socio>socio = new ArrayList<>();
		assertEquals(0,socio.size());
		socio.add(null);
		socio.add(null);
		socio.add(null);
		socio.add(null);
		assertEquals(4,socio.size());
		socio.remove(null);
		socio.remove(null);
		assertEquals(2,socio.size());
	
		
	}

}
