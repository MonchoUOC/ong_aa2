package DAO;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import ClasesGenericas.Proyecto;
import ClasesGenericas.Proyectos;

public class XmlProyectos implements IProyectos {
	
	private JAXBContext jaxbContext = null;
	private String nombreFichero = null;

	public XmlProyectos() throws JAXBException {
		this.jaxbContext = JAXBContext.newInstance(Proyectos.class);
		this.nombreFichero = "Proyectos.xml";
	}
	
		
	@Override
	public void guardarProyectos(Proyectos proyectos) throws JAXBException {
		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.marshal(proyectos, new File(nombreFichero));
		System.out.println();
		System.out.println("Se ha escrito el fichero " + nombreFichero + " con el siguiente contenido:");
		System.out.println();
		marshaller.marshal(proyectos, System.out);
	}

	@Override
	public Proyectos listarProyectos() throws JAXBException {
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		Proyectos proyectos = (Proyectos) unmarshaller.unmarshal( new File(nombreFichero));
	    System.out.println();
		System.out.println("Estos son los proyectos que tenemos  en el fichero " + nombreFichero);
		
	    for(Proyecto proyecto : proyectos.getProyectos())
	    {
	    	System.out.println("-----------------------------------------------------");
	        System.out.println("Nombre del Proyecto: \t" + proyecto.getNombreProyecto());
	        System.out.println("Pais: \t" + proyecto.getPais());
	        System.out.println("Localizacion: \t" + proyecto.getLocalizacion());
	        System.out.println("Fecha de inicio: \t" + proyecto.getFechaInicio());
	        System.out.println("Fecha de finalización: \t" + proyecto.getFechaFinalizacion());
	        System.out.println("Socio Local: \t" + proyecto.getSocioLocal());
	        System.out.println("Financiador: \t" + proyecto.getFinanciador());
	        System.out.println("Financiacion aportada: \t" + proyecto.getFinanciacionAportada());
	        System.out.println("Codigo de proyecto: \t" + proyecto.getCodigoProyecto());
	        System.out.println("-----------------------------------------------------");
	      
	    }
	    
		return null;
	}

}