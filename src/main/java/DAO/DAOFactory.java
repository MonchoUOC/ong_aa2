package DAO;

import javax.xml.bind.JAXBException;

public abstract class DAOFactory {

	// Tipos de DAO soportados por la factorÃ­a
	public static final int XML = 1;
	//public static final int MYSQL = 2;

	
	public abstract XmlProyectos getProyectos() throws JAXBException;
	public abstract XmlSocios getSocios() throws JAXBException;
	public abstract XmlVoluntarios getVoluntarios()throws JAXBException;
	
	
	public static DAOFactory getDAOFactory(int whichFactory) {

		switch (whichFactory) {
		case XML:
			return new XmlDAOFactory();
		// case MYSQL:
		// return new MySqlDAOFactory();
		default:
			return null;
		}
	}

}