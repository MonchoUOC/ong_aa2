package DAO;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import ClasesGenericas.Voluntarios;
import ClasesGenericas.Voluntario;

public class XmlVoluntarios  implements IVoluntarios{
	
	private JAXBContext jaxbContext=null;
	private String nombreFichero = null;
	
	public XmlVoluntarios()throws JAXBException{
		this.jaxbContext = JAXBContext.newInstance(Voluntarios.class);
		this.nombreFichero = "Voluntarios.xml";
		
	}
	
	public void guardarVoluntarios (Voluntarios voluntarios)throws JAXBException{
		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);
		marshaller.marshal(voluntarios, new File(nombreFichero));
		System.out.println();
		System.out.println("Se ha escrito el fichero"+ nombreFichero + "con el siguiente contenido");
		System.out.println();
		marshaller.marshal(voluntarios, System.out);
		
	}
	
	public Voluntarios listarVolunatrios() throws JAXBException{
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		Voluntarios voluntarios = (Voluntarios) unmarshaller.unmarshal(new File(nombreFichero));
		System.out.println();
		System.out.println("Estos son los voluntarios que tenemos en el fichero"+ nombreFichero);
		
		for(Voluntario voluntario : voluntarios.getVoluntarios()) {
			
			System.out.println("-------------------------------------------------------------");
			System.out.println("Numero de voluntario: \t"+ voluntario.getNumeroVoluntario());
			System.out.println("Fecha de ingreso: \t"+ voluntario.getFechaIngreso());
            System.out.println("Nombre: \t"+voluntario.getNombre());
			System.out.println("Apellidos: \t"+voluntario.getApellidos());
			System.out.println("Dirección: \t"+voluntario.getDireccion());
			System.out.println("Correo Elecgtronico: \t"+voluntario.getCorreoElectronico());
			System.out.println("Pais:  \t" +voluntario.getPais());
			System.out.println("Tipo de voluntario:  \t"+voluntario.getTipoDeVoluntario());
			System.out.println("-------------------------------------------------------------");
			
		}
		
		return null;
		}

	@Override
	public Voluntarios listarVoluntarios() throws JAXBException {
		// TODO Auto-generated method stub
		return null;
	}

}
