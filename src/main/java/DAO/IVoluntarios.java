package DAO;
import javax.xml.bind.JAXBException;

import ClasesGenericas.Voluntarios;


public interface IVoluntarios {
	public void guardarVoluntarios(Voluntarios voluntarios) throws JAXBException;
	public Voluntarios listarVoluntarios() throws JAXBException;
	
	

}