package ClasesGenericas;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
@XmlRootElement(name = "socio")
@XmlAccessorType(XmlAccessType.FIELD)

public class Voluntario {
	@XmlAttribute(name = "numeroVoluntario")
	 int numeroVoluntario;
	@XmlElement(name = "fechaIngeso")
	 String fechaIngreso;
	
	@XmlElement(name ="nombre")
	 String nombre;
	@XmlElement(name="apellidos")
	 String apellidos;
	@XmlElement(name="direccion")
	 String direccion;
	@XmlElement(name="correoElectronico")
	 String correoElectronico;
	@XmlElement(name="pais")
	String pais;
	@XmlElement(name="tipoDeVoluntario")
	String tipoDeVoluntario;
	//CONSTRUCTOR
	
	public Voluntario(int numeroVoluntario, String fechaIngreso, String nombre, String apellidos, String direccion,
			String correoElectronico, String pais, String tipoDeVoluntario) {
		super();
		this.numeroVoluntario = numeroVoluntario;
		this.fechaIngreso = fechaIngreso;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.direccion = direccion;
		this.correoElectronico = correoElectronico;
		this.pais = pais;
		this.tipoDeVoluntario = tipoDeVoluntario;
	
	}
	
	
	public Voluntario() {
		super();
	}


	//ACCESADORES
	public int getNumeroVoluntario() {
		return numeroVoluntario;
	}
	
	
	public void setNumeroVoluntario(int numeroVoluntario) {
		this.numeroVoluntario = numeroVoluntario;
	}
	public String getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getTipoDeVoluntario() {
		return tipoDeVoluntario;
	}
	public void setTipoDeVoluntario(String tipoDeVoluntario) {
		this.tipoDeVoluntario = tipoDeVoluntario;
	}
	
	
	@Override
	public String toString() {
		return "Voluntario [numeroVoluntario=" + numeroVoluntario + ", fechaIngreso=" + fechaIngreso + ", nombre="
				+ nombre + ", apellidos=" + apellidos + ", direccion=" + direccion + ", correoElectronico="
				+ correoElectronico + ", pais=" + pais + ", tipoDeVoluntario=" + tipoDeVoluntario + "]";
	}
	
	
	
	
	
}