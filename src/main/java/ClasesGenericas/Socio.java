package ClasesGenericas;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.aspectj.weaver.tools.cache.AsynchronousFileCacheBacking.RemoveCommand;

import javax.xml.bind.annotation.XmlElement;
@XmlRootElement(name = "socio")
@XmlAccessorType(XmlAccessType.FIELD)

public class Socio {
	@XmlAttribute(name = "numeroSocio")
	 int numeroSocio;
	@XmlElement(name = "fechaIngeso")
	 String fechaIngreso;
	@XmlElement(name ="tipoCuota")
	 String tipoCuota;
	@XmlElement(name ="nombre")
	 String nombre;
	@XmlElement(name="apellidos")
	 String apellidos;
	@XmlElement(name="direccion")
	 String direccion;
	@XmlElement(name="correoElectronico")
	 String correoElectronico;
	
	 
	 //CONSTRUCTOR
	
	public Socio(int numeroSocio, String fechaIngreso, String tipoCuota, String nombre, String apellidos,
			String direccion, String correoElectronico) {
		super();
		this.numeroSocio = numeroSocio;
		this.fechaIngreso = fechaIngreso;
		this.tipoCuota = tipoCuota;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.direccion = direccion;
		this.correoElectronico = correoElectronico;
	}
	public Socio () {
		super();
		
	}
	
	
	
	public int getNumeroSocio() {
		return numeroSocio;
	}
	public void setNumeroSocio(int numeroSocio) {
		this.numeroSocio = numeroSocio;
	}
	public String getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public String getTipoCuota() {
		return tipoCuota;
	}
	public void setTipoCuota(String tipoCuota) {
		this.tipoCuota = tipoCuota;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	
	@Override
	public String toString() {
		return "Socio [numeroSocio=" + numeroSocio + ", fechaIngreso=" + fechaIngreso + ", tipoCuota=" + tipoCuota
				+ ", nombre=" + nombre + ", apellidos=" + apellidos + ", direccion=" + direccion
				+ ", correoElectronico=" + correoElectronico + "]";
	}
	
	
	

}