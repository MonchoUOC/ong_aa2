package ClasesGenericas;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.aspectj.weaver.tools.cache.AsynchronousFileCacheBacking.RemoveCommand;
import org.springframework.context.support.StaticApplicationContext;

import java.util.ArrayList;

import java.util.List;

@XmlRootElement(name = "proyectos")
public class Proyectos {
	List<Proyecto> proyectos;

	@XmlElement(name = "proyecto")
	public List<Proyecto> getProyectos() {
		return proyectos;
	}

	public void setProyectos(List<Proyecto> proyectos) {
		this.proyectos = proyectos;
	}

	public void agregarProyecto(Proyecto proyecto) {
		if (this.proyectos == null) {
			this.proyectos = new ArrayList<Proyecto>();
		}
		this.proyectos.add(proyecto);

	}

	public void eliminarProyecto(String codigo) {
		for (int i = 0; i < proyectos.size(); i++) {
			Proyecto proyecto = proyectos.get(i);
			if (codigo.equals(proyecto.codigoProyecto)) {
				proyectos.remove(proyecto);
			}
		}
	}
}
		
		
		

	
	
	
	
	
	
	


	
