
package ClasesGenericas;




import javax.xml.bind.annotation.*;

@XmlRootElement(name = "Proyectos")
@XmlAccessorType(XmlAccessType.FIELD)

public class Proyecto {
	@XmlElement(name = "nombreProyecto")	
String nombreProyecto;
	@XmlElement(name = "pais")	
String pais;
	@XmlElement(name = "localizacion")	
String localizacion;
	@XmlElement(name = "fechaInicio")	
String fechaInicio;
	@XmlElement(name = "fechaFinalizacion")	
String fechaFinalizacion;
	@XmlElement(name = "socioLocal")	
String socioLocal;
	@XmlElement(name = "financiador")
String financiador;
	@XmlElement(name = "financiacionAportada")	
Float financiacionAportada;
    @XmlAttribute(name = "codigoProyecto")
String codigoProyecto;

//CONSTRUCTOR
public Proyecto(String nombreProyecto, String pais, String localizacion, String fechaInicio, String fechaFinalizacion, String socioLocal, String financiador, float financiacionAportada,
		String codigoProyecto) {
	super();
	this.nombreProyecto = nombreProyecto;
	this.pais = pais;
	this.localizacion = localizacion;
	this.fechaInicio = fechaInicio;
	this.fechaFinalizacion = fechaFinalizacion;
	this.socioLocal = socioLocal;
	this.financiador = financiador;
	this.financiacionAportada = financiacionAportada;
	this.codigoProyecto = codigoProyecto;
}

public Proyecto() {
	
}

//METODOS ACCESORES
public String getNombreProyecto() {
	return nombreProyecto;
}

public void setNombreProyecto(String nombreProyecto) {
	this.nombreProyecto = nombreProyecto;
}

public String getPais() {
	return pais;
}

public void setPais(String pais) {
	this.pais = pais;
}

public String getLocalizacion() {
	return localizacion;
}

public void setLocalizacion(String localizacion) {
	this.localizacion = localizacion;
}

public String getFechaInicio() {
	return fechaInicio;
}

public void setFechaInicio(String fechaInicio) {
	this.fechaInicio = fechaInicio;
}

public String getFechaFinalizacion() {
	return fechaFinalizacion;
}

public void setFechaFinalizacion(String fechaFinalizacion) {
	this.fechaFinalizacion = fechaFinalizacion;
}

public String getSocioLocal() {
	return socioLocal;
}

public void setSocioLocal(String socioLocal) {
	this.socioLocal = socioLocal;
}

public String getFinanciador() {
	return financiador;
}

public void setFinanciador(String financiador) {
	this.financiador = financiador;
}

public Float getFinanciacionAportada() {
	return financiacionAportada;
}

public void setFinanciacionAportada(Float financiacionAportada) {
	this.financiacionAportada = financiacionAportada;
}

public String getCodigoProyecto() {
	return codigoProyecto;
}

public void setCodigoProyecto(String codigoProyecto) {
	this.codigoProyecto = codigoProyecto;
}

@Override
public String toString() {
	return "Proyectos [nombreProyecto=" + nombreProyecto + ", pais=" + pais + ", localizacion=" + localizacion
			+ ", fechaInicio="	+ fechaInicio + ", fechaFinalizacion=" + fechaFinalizacion + ", socioLocal=" + socioLocal + ", financiador="
			+ financiador + ", financiacionAportada=" + financiacionAportada + ", codigoProyecto=" + codigoProyecto
			+ "]";
}



	
	
}




