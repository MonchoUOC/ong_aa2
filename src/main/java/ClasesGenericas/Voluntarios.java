package ClasesGenericas;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="voluntarios")
public class Voluntarios {
	List<Voluntario>voluntarios;

	@XmlElement(name="voluntario")
	public List <Voluntario>getVoluntarios(){
		return voluntarios;
		}

	public void setVoluntarios(List<Voluntario> voluntarios) {
		this.voluntarios = voluntarios;
	}
	
	public void agregarVoluntario(Voluntario voluntario) {
		if(this.voluntarios == null) {
			this.voluntarios= new ArrayList<Voluntario>();
		}
		this.voluntarios.add(voluntario);
	}
	
	public void eliminarVoluntario(int numero) {
		for (int i = 0; i < voluntarios.size(); i++) {
		   Voluntario voluntario = voluntarios.get(i);
			if (numero == voluntario.numeroVoluntario) {
		         voluntarios.remove(voluntario);
		    }     
		}
	

	}
	
}
